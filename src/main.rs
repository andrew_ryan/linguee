mod translate;
#[allow(warnings)]
fn main() {
    fn run() {
        use cok::cli::Cli;
        Cli::load_from_json("./cli.json");
        let args = Cli::args();  
        if args.len()>1{
            println!("linguee only one word parameter is required.\nExample:\nlinguee list => translate list to Chinese\nlinguee 列表 => translate 列表 to English")
        } 
        else if args.len() == 1 && args[0] !="-h" && args[0] !="--help" {
            let text = &args[0];
            let translete_to = "en";
            match translate::translate_auto(text, translete_to.to_owned()) {
                Ok(r) => {
                    println!("{}", r);
                }
                Err(r) => {
                    println!("{}", r);
                }
            }
        }
    }
    run();
}
