use tl;
pub struct Translator<'a> {
    pub to: &'a str,
    pub from: &'a str,
}

impl<'a> Translator<'a> {
    pub fn translate(&self, text: &str) -> Result<String, String> {
        parse_result(fetch_page(text, self.from, self.to))
    }
}

fn fetch_page(text: &str, _from: &str, _to: &str) -> Result<String, String> {
    let formatted_url = format!(
        "http://www.linguee.com/english-chinese/search?query={}",
        text
    );
    match tomcat::get_blocking(formatted_url) {
        Ok(response) => Ok(response.text),
        Err(err) => return Err(err.to_string()),
    }
}
use doe;
#[allow(warnings)]
fn parse_result(result: Result<String, String>) -> Result<String, String> {
    match result {
        Ok(body) => {
            let dom = tl::parse(&body, tl::ParserOptions::default()).unwrap();
            let parser = dom.parser();
            let translate_result_element = dom.get_elements_by_class_name("exact");
            
            for translate_result_node in translate_result_element {
                


                let translate_result_node_string = translate_result_node
                    .get(parser)
                    .unwrap()
                    .inner_text(parser)
                    .replace("\r\n", "");
                let translate_result_vec = translate_result_node_string
                    .split("&middot")
                    .map(|s| {
                        let s_vec = doe::split_to_vec!(
                            s.trim()
                                .replace("()", "")
                                .replace("", "").replace("&#039;", "'"),
                            "\n"
                        );
                        s_vec.join("\n")
                    })
                    .collect::<Vec<_>>();
                let translate_result_vec_1 = translate_result_vec.iter().map(|s|{
                    let mut re = "".to_string();
                    if s.contains("&mdash"){
                        re.push_str(&s.replace("&mdash", ""));
                        re.push_str("\r\r");
                    }else{
                        re.push_str(s);
                    }
                    re
                }).collect::<Vec<_>>();
                let translate_result_vec_2 = doe::split_to_vec!( translate_result_vec_1.join(""),"\r\r");
                // println!("{}", translate_result_vec.join("\n"));
                println!("{}", translate_result_vec_2.join("").replace("  ;", " ").replace("&nbsp;", ""));
            }

            Ok("".to_string())
        }
        Err(err) => return Err(err),
    }
}

pub fn translate_auto(text: &str, to: String) -> Result<String, String> {
    let translator_struct = Translator {
        to: &to,
        from: "auto",
    };

    translator_struct.translate(text)
}
