# linguee is a English-Chinese command line dictionary
[![Crates.io](https://img.shields.io/crates/v/linguee.svg)](https://crates.io/crates/linguee)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/linguee)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/linguee/-/raw/master/LICENSE)
## Cli:
```
linguee 0.1.6
Anonymous <dnrops@anonymous.com>
linguee is a English-Chinese command line dictionary
Example:
linguee list => translate list to Chinese
linguee 列表 => translate 列表 to English

USAGE:
        OPTION          REQUIRED        ABOUT
linguee  word           true            A word to be translated
linguee -h --help                       Prints help information
```

## install
```
cargo install linguee
```

## download bin
``` 
#linux
wget https://gitlab.com/andrew_ryan/linguee/-/raw/master/bin/linguee
#windows
wget https://gitlab.com/andrew_ryan/linguee/-/raw/master/bin/linguee.exe
```